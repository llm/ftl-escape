<?php
/**
 * @Entity @Table(name="planets")
 **/
 
 use Doctrine\Common\Collections\ArrayCollection;
 
class Planet
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text") **/
    protected $name;
    /** @Column(type="array",nullable=true) **/
    protected $garrison=array();
    /** @Column(type="integer", options={"default"=0}) **/
    protected $materials=0;
    /** @Column(type="integer", options={"default"=0}) **/
    protected $materialproduction=1;
    /** @Column(type="boolean") **/
    protected $isattacked=false;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $initialpopulation=0;
    /** @Column(type="integer",options={"default"=1}) **/
    protected $status=1;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $inteldate=0;
    /** @Column(type="array",nullable=true) **/
    protected $knowngarrison=array();
    /** @OneToOne (targetEntity="Sector",inversedBy="planet") **/
    private $sector;
    /** @ManyToOne (targetEntity="Player") **/
    private $freedBy;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $attackcount=0;
    /** @Column(type="boolean",options={"default"=1}) **/
    protected $initialcolony=true;
    /**
     * @OneToMany(targetEntity="PlanetaryEquipment", mappedBy="planet", indexBy="id")
     * @var PlanetaryEquipment[]
     **/
    private $equipments;
    /** @Column(type="integer",options={"default"=10}) **/
    protected $maxequipments = 10;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $population = 0;
    
    /*
     * garrison
     * array(EnemyShipTypeId=>nb)
     */
    
    /*
     *  status
     *  0 : freed
     *  1 : occupied
     *  2 : under allied attack
     *  3 : under enemy attack
     *  4 : destroyed
     */
    
    public function __construct($name,$materialproduction)
    {
        $this->equipments = new ArrayCollection();
        $this->name = $name;
        $this->materialproduction = $materialproduction;
    }
    
    public function setSector($sector)
    {
        $this->sector = $sector;
    }
    
    public function getSector()
    {
        return $this->sector;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getFreedBy()
    {
        return $this->freedBy;
    }
    
    public function getColonizedBy()
    {
        return $this->freedBy;
    }
    
    public function setFreedBy($player)
    {
        $this->freedBy = $player;
    }
    
    public function setColonizedBy($player)
    {
        $this->freedBy = $player;
    }
    
    public function setMaterial($material)
    {
        $this->materials = $material;
    }
    
    public function getMaterial()
    {
        return $this->materials;
    }
    
    public function setMaterialProduction($production)
    {
        $this->materialproduction = $production;
    }
    
    public function getMaterialProduction()
    {
        $production = $this->materialproduction;
        $equipments = $this->equipments;
        foreach ($equipments as $equipment)
        {
            $production += $equipment->getType()->getMaterialProduction();
        }
        return $production;
    }
    
    public function getFoodProduction()
    {
        $production = 0;
        $equipments = $this->equipments;
        foreach ($equipments as $equipment)
        {
            $production += $equipment->getType()->getFoodProduction();
        }
        return $production;
    }
    
    public function getFuelProduction()
    {
        $production = 0;
        $equipments = $this->equipments;
        foreach ($equipments as $equipment)
        {
            $production += $equipment->getType()->getFuelProduction();
        }
        return $production;
    }
    
    public function getUraniumProduction()
    {
        $production = 0;
        $equipments = $this->equipments;
        foreach ($equipments as $equipment)
        {
            $production += $equipment->getType()->getUraniumProduction();
        }
        return $production;
    }
    
    public function getMedicineProduction()
    {
        $production = 0;
        $equipments = $this->equipments;
        foreach ($equipments as $equipment)
        {
            $production += $equipment->getType()->getMedicineProduction();
        }
        return $production;
    }
    
    public function getStaffProduction()
    {
        $production = 0;
        $equipments = $this->equipments;
        foreach ($equipments as $equipment)
        {
            $production += $equipment->getType()->getStaffProduction();
        }
        return $production;
    }
    
    public function getBirthProduction()
    {
        $production = 0;
        $equipments = $this->equipments;
        foreach ($equipments as $equipment)
        {
            $production += $equipment->getType()->getBirthRate();
        }
        $production = $this->getPopulation() < $production ? round($this->getPopulation() / 4) : $production;
        return $production;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function isAttacked()
    {
        return $this->isattacked;
    }
    
    public function attack()
    {
        $this->isattacked = true;
        $this->status = 2;
        $this->attackcount = $this->attackcount + 1;
    }
    
    public function unattack()
    {
        $this->isattacked = false;
    }
    
    public function setGarrison($array)
    {
        $this->garrison = $array;
    }
    
    public function getKnownGarrison()
    {
        return $this->knowngarrison;
    }
    
    public function getGarrison()
    {
        return $this->garrison;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
    }
    
    public function getStatus()
    {
        return $this->status;
    }
    
    public function spy()
    {
        $this->inteldate = time();
        $this->knowngarrison = $this->garrison;
    }
    
    public function getAttackCount()
    {
        return $this->attackcount;
    }
    
    public function reinitAttackCount()
    {
        $this->attackcount = 0;
    }
    
    public function getIntelDate()
    {
        return $this->inteldate;
    }
    
    public function setInitialColony($value)
    {
        $this->initialcolony=$value;
    }
    
    public function getInitialColony()
    {
        return $this->initialcolony;
    }
    
    public function getEquipments()
	{
		return $this->equipments->toArray();
	}

	public function getEquipment($id)
	{
		if (!isset($this->equipments[$id]))
		{
			echo "not in equipments";
			return false;
		}
		else
		{
			return $this->equipments[$id];
		}
	}

	public function addEquipment($equipment)
	{
        if (count($this->equipments) < $this->maxequipments)
        {
            $this->equipments[$equipment->getId()] = $equipment;
        }
	}
    
    public function getMaxEquipments()
    {
        return $this->maxequipments;
    }
    
    public function setMaxEquipments($value)
    {
        $this->maxequipments = $value;
    }
    
    public function setPopulation($value)
    {
        $this->population = $value;
    }
    
    public function getPopulation()
    {
        return $this->population;
    }
    
}
