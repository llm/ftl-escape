<?php
/**
 * @Entity @Table(name="game")
 **/
class Game
{
	/** @Id @Column(type="integer") **/
	protected $id;
	/** @Column(type="boolean") **/
	protected $init=false;
	/** @Column(type="boolean", options={"default" = 1}) **/
	protected $signup=true;
	/** @Column(type="boolean", options={"default" = 0}) **/
	protected $earthfound=false;
	/** @Column(type="boolean", options={"default" = 0}) **/
	protected $coloniesretaken=false;
	/** @Column(type="boolean", options={"default" = 0}) **/
	protected $permanentdeath=false;
	/** @Column(type="boolean", options={"default" = 1}) **/
	protected $choosedifficulty=true;
	/** @Column(type="integer", options={"default" = 0}) **/
	protected $stage=0;
	/** @Column(type="boolean", options={"default" = 1}) **/
	protected $chooseobjective=true;
	
	public function __construct()
	{
		$this->id=1;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getInit()
	{
		return $this->init;
	}
	
	public function init()
	{
		$this->init = true;
	}
	
	public function setSignup($signup)
	{
		$this->signup = $signup;
	}
	
	public function getSignup()
	{
		return $this->signup;
	}
	
	public function hasFoundEarth()
	{
		return $this->earthfound;
	}
	
	public function foundEarth($value=false)
	{
		$this->earthfound = $value;
	}
	
	public function getColoniesRetaken()
	{
		return $this->coloniesretaken;
	}
	
	public function setColoniesRetaken($value)
	{
		$this->coloniesretaken=$value;
	}
	
	public function getPermanentDeath()
	{
		return $this->permanentdeath;
	}
	
	public function setPermanentDeath($value)
	{
		$this->permanentdeath=$value;
	}
	
	public function setStage($value)
	{
		$this->stage=$value;
	}
	
	public function getStage()
	{
		return $this->stage;
	}
	
	public function setChooseDifficulty($value)
	{
		$this->choosedifficulty=$value;
	}
	
	public function getChooseDifficulty()
	{
		return $this->choosedifficulty;
	}
	
	public function setChooseObjective($value)
	{
		$this->chooseobjective=$value;
	}
	
	public function getChooseObjective()
	{
		return $this->chooseobjective;
	}
	
	public function getCanDeleteAccount()
	{
		return $this->getStage() == STAGE_INITIAL;
	}
}
