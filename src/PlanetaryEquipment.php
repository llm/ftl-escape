<?php
require_once __DIR__.'/../const.php';
/**
 * @Entity @Table(name="planetaryequipments")
 **/
class PlanetaryEquipment
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @ManyToOne(targetEntity="PlanetaryEquipmentType") **/
    private $type;
    /** @Column(type="integer") **/
    protected $hp;
    /** @ManyToOne(targetEntity="Planet", inversedBy="equipments")
     * @var Planet
     */
    private $planet;
    
    public function __construct($planet,$type)
    {
		if (count($planet->getEquipments()) >= $planet->getMaxEquipments())
		{
			throw new Exception('Illegal planetary equipment addition');
		}
		$this->planet=$planet;
		$this->type=$type;
		$this->hp = $type->getMaxHP();
		$planet->addEquipment($this);
	}
    
    public function getId()
    {
		return $this->id;
	}
	
	public function getPlanet()
	{
		return $this->planet;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function getHP()
	{
		return $this->hp;
	}
	
	public function setHP($value)
	{
		$this->hp = $value;
	}
}
