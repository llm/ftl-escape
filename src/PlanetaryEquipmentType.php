<?php
require_once __DIR__.'/../const.php';
/**
 * @Entity @Table(name="planetaryequipmenttypes")
 **/
class PlanetaryEquipmentType
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text", unique=true) **/
    protected $name;
	/** @Column(type="integer",options={"default"=10000}) **/
	protected $price=0;
	/** @Column(type="integer",options={"default"=100}) **/
	protected $maxhp=100;
    /** @Column(type="boolean",options={"default"=0}) **/
    protected $buildable=true;
    /** @Column(type="boolean",options={"default"=1}) **/
    protected $uniqueEquipment=false;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $materialproduction=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $foodproduction=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $fuelproduction=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $medicineproduction=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $birthrate=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $staffproduction=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $uraniumproduction=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $defense=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $attack=0;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $orbitaldefense=0;

    public function getId()
    {
		return $this->id;
	}

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getDescription()
	{
		return $this->name.'.description';
	}

	public function getBuildable()
	{
		return $this->buildable;
	}

	public function isBuildable()
	{
		return $this->getBuildable();
	}

	public function setBuildable($buildable)
	{
		$this->buildable = $buildable;
	}

	public function isUnique()
	{
		return $this->uniqueEquipment;
	}

	public function setUnique($unique)
	{
		$this->uniqueEquipment = $unique;
	}

	public function setPrice($price)
	{
		$this->price=$price;
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getFormatedPrice($decPoint='.',$thousandsSep=' ')
	{
		return Tools::getFormatedNumber($this->getPrice());
	}
	
	public function getMaterialProduction()
	{
		return $this->materialproduction;
	}
	
	public function setMaterialProduction($value)
	{
		$this->materialproduction = $value;
	}
	
	public function getFoodProduction()
	{
		return $this->foodproduction;
	}
	
	public function setFoodProduction($value)
	{
		$this->foodproduction = $value;
	}
	
	public function getFuelProduction()
	{
		return $this->fuelproduction;
	}
	
	public function setFuelProduction($value)
	{
		$this->fuelproduction = $value;
	}
	
	public function getMedicineProduction()
	{
		return $this->medicineproduction;
	}
	
	public function setMedicineProduction($value)
	{
		$this->medicineproduction = $value;
	}
	
	public function getStaffProduction()
	{
		return $this->staffproduction;
	}
	
	public function setStaffProduction($value)
	{
		$this->staffproduction = $value;
	}
	
	public function getUraniumProduction()
	{
		return $this->uraniumproduction;
	}
	
	public function setUraniumProduction($value)
	{
		$this->uraniumproduction = $value;
	}
	
	public function getBirthRate()
	{
		return $this->birthrate;
	}
	
	public function setBirthRate($value)
	{
		$this->birthrate = $value;
	}
	
	public function getDefense()
	{
		return $this->defense;
	}
	
	public function setDefense($value)
	{
		$this->defense = $value;
	}
	
	public function getAttack()
	{
		return $this->attack;
	}
	
	public function setAttack($value)
	{
		$this->attack = $value;
	}
	
	public function getMaxHP()
	{
		return $this->maxhp;
	}
	
	public function setMaxHP($value)
	{
		$this->maxhp = $value;
	}
	
	public function getOrbitalDefense()
	{
		return $this->orbitaldefense;
	}
	
	public function setOrbitalDefense($value)
	{
		$this->orbitaldefense = $value;
	}
}
