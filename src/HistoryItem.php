<?php
/**
 * @Entity @Table(name="historyitem")
 **/
 
 use Doctrine\Common\Collections\ArrayCollection;
 
class HistoryItem
{
	/** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text") **/
    protected $name;
    /** @ManyToOne(targetEntity="ShipType") **/
    private $type;
    /** @Column(type="integer") **/
    protected $passengers;
    /** @Column(type="integer") **/
    protected $staff;
    /** @ManyToOne(targetEntity="Fleet") **/
    private $fleet;
    /** @ManyToOne(targetEntity="EnnemyShipType") nullable=true **/
    private $ennemyType;
    /** @Column(type="integer") **/
    protected $time=0;
    
    public function __construct($ship,$killer=null)
    {
		$this->name = $ship->getName();
		$this->type = $ship->getType();
		$this->passengers = $ship->getPassengers();
		$this->staff = $ship->getStaff();
		$this->fleet = $ship->getFleet();
		if (!is_null($killer))
			$this->ennemyType = $killer->getType();
		$this->time = time();
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function getPassengers()
	{
		return $this->passengers;
	}
	
	public function getStaff()
	{
		return $this->staff;
	}
	
	public function getKiller()
	{
		return $this->ennemyType;
	}
	
	public function getTime()
	{
		return $this->time;
	}
}
