<?php

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$player = $entityManager->getRepository('Player')->find($_POST['id']);
$i18n = new I18n();
$i18n->autoSetLang();

if (is_numeric($_POST['nb']) && $_POST['nb'] > 0)
{
    $sector = $player->getSector();
    Builder::populateSector($sector,$_POST['nb']);
}

$entityManager->flush();
header('Location: index.php');