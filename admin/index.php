<!DOCTYPE html>
<html>
<head>
	<title>Admin console</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=yes"/>
	<link rel="stylesheet" href="css/main.css" type="text/css"/>
</head>
<body>
	<div class="datagrid">
		<?php
		
		require_once(__DIR__.'/../bootstrap.php');
		require_once(__DIR__.'/../const.php');
		require_once(__DIR__.'/../tools.php');
		require_once(__DIR__.'/../lib/i18n.php');
		
		$players = $entityManager->getRepository('Player')->findAll();
		$i18n = new I18n();
		$i18n->autoSetLang();
		?>
		<table>
			<thead>
			<tr><th>Nom (last connection)</th><th>Difficulté</th><th>Nb ships</th><th>Stocks</th><th>Survivors</th><th>Sector</th></tr>
			</thead>
			<tbody>
			<?php
			foreach ($players as $player)
			{
				$fleet = $player->getFleet();
				$difficulty = Helper::getDifficulty($player);
				echo '<tr><td>';
				if ($player->isGameOver())
				{
					echo '<font color="#FF0000">';
				}
				$lastlogin = $player->getLastLogin() == 0 ? "N/A" : date('d/m/y H:i:s',$player->getLastLogin()) ;
				echo $player->getLogin().' ('.$lastlogin.')';
				if ($player->isGameOver())
				{
					echo '</font>';
				}
				echo '</td><td>';
				echo $i18n->getText('lbl.difficulty.'.$difficulty);
				echo '</td><td>';
				echo count($player->getFleet()->getShips());
				?>
				<a href="addship.php?playerid=<?php echo $player->getId(); ?>">+</a>
				<?php
				echo '</td><td>';
				?>
				<form action="modifystocks.php" method="post">
					<input type="hidden" name="id" value="<?php echo $player->getId();?>"/>
					<dl>
						<dt>Food</dt><dd><input type="number" name="food" value="<?php echo $fleet->getFood(); ?>"/></dd>
						<dt>Fuel</dt><dd><input type="number" name="fuel" value="<?php echo $fleet->getFuel(); ?>"/></dd>
						<dt>Material</dt><dd><input type="number" name="material" value="<?php echo $fleet->getMaterial(); ?>"/></dd>
						<dt>Uranium</dt><dd><input type="number" name="uranium" value="<?php echo $fleet->getUranium(); ?>"/></dd>
						<dt>Moral</dt><dd><input type="number" name="moral" value="<?php echo $fleet->getMoral(); ?>"/></dd>
						<dt>Medicine</dt><dd><input type="number" name="medicine" value="<?php echo $fleet->getMedicine(); ?>"/></dd>
						<dt>Staff</dt><dd><input type="number" name="staff" value="<?php echo $fleet->getQualifiedStaff(); ?>"/></dd>
					</dl>
				<input type="submit"/>
				</form>
				<?php
				echo '</td><td>';
				$survivors = 0;
				$ships = $player->getFleet()->getShips();
				foreach ($ships as $ship)
				{
					$survivors += $ship->getPassengers() + $ship->getStaff();
				}
				echo number_format($survivors,0,'.',' ');
				echo '</td><td>';
				$sector = $player->getSector();
				if (is_null($sector))
				{
					echo 'NO SECTOR <a href="createsector.php?playerid='.$player->getId().'">Create</a>';
				}
				else
				{
					echo $sector->getWrecks().' wrecks<br/>';
					echo count($sector->getEnnemies()).' ennemies<br/>';
					?>
					Add ennemies : <form action="addennemies.php" method="post"><input type="number" name="nb"/><input type="hidden" name="id" value="<?php echo $player->getId();?>"/><input type="submit"/></form>
					<?php
				}
				echo '</td>';
				
				echo '</tr>';
			}
			
			?>
			</tbody>
		</table>
	</div>
</body>
</html>
