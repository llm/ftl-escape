<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$qb = $entityManager->createQueryBuilder();

$qb->update('Player','p')
	->set('p.assassinationcount',0);
	
$qb->getQuery()->execute();

$entityManager->flush();