<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$game = $entityManager->getRepository('Game')->find(1);

$stage = $game->getStage();
if ($stage == STAGE_COUNTER_ATTACK_ONE)
{
    $nexii = $entityManager->getRepository('Nexus')->findAll();
    foreach ($nexii as $nexus)
    {
        if (!$nexus->isAttacked() && $nexus->getStatus() != 4)
        {
            // @TODO : change this
            $nbAvailableShips = count($entityManager->getRepository('EnnemyShipType')->findAll());
            $randId = rand(1,$nbAvailableShips);
            
            $garrison = $nexus->getGarrison();
            $garrison[$randId] = $garrison[$randId] + 1;
            $nexus->setGarrison($garrison);
        }
    }
}

$entityManager->flush();