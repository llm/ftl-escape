<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$game = $entityManager->getRepository('Game')->find(1);

$stage = $game->getStage();
if ($stage == STAGE_INITIAL)
{
    $qb = $entityManager->createQueryBuilder();
    $qb->select('p')
        ->from('Planet','p')
        ->where('p.initialcolony = true')
        ->andWhere('p.freedBy is null');
    $planets = $qb->getQuery()->getResult();
    if (count($planets) == 0)
    {
        $game->setStage(STAGE_COUNTER_ATTACK_ONE);
        $players = $entityManager->getRepository('Player')->findAll();
        foreach ($players as $player)
        {
			$player->setObjectiveType(OBJECTIVE_FINAL_ID);
            $player->addGlobalAnnouncement(ANNOUNCEMENT_CA_ONE);
        }
    }
}
elseif ($stage == STAGE_COUNTER_ATTACK_ONE)
{
    $qb = $entityManager->createQueryBuilder();
    $qb->select('n')
        ->from('Nexus','n')
        ->where('n.destroyedBy is null');
    $nexuss = $qb->getQuery()->getResult();
    if (count($nexuss) == 0)
    {
        $game->setStage(STAGE_COUNTER_ATTACK_TWO);
        $game->setPermanentDeath(true);
        $game->setSignup(false);
        $game->setChooseDifficulty(false);
        $game->setChooseObjective(false);
        $players = $entityManager->getRepository('Player')->findAll();
        foreach ($players as $player)
        {
            $player->addGlobalAnnouncement(ANNOUNCEMENT_CA_TWO);
            $player->setObjectiveType(OBJECTIVE_FINAL_ID);
            $fleet = $player->getFleet();
            $ships = $fleet->getShips();
            foreach ($ships as $ship)
            {
				$type = $ship->getType();
				if ($type->getDamage() == 0 && !$type->canRepair())
				{
					Helper::destroyShip($ship,false);
					$entityManager->remove($ship);
				}
				else
				{
					$ship->setPassengers(0);
					$ship->setStaff($type->getQualifiedStaff());
					$ship->setLevel(SHIP_UPGRADE_MAX_LEVEL);
					$ship->repairFTL();
					$ship->repairSurviveSystems();
					$ship->setHP($type->getMaxHP($ship->getLevel()));
				}
			}
        }
    }
}
elseif ($stage == STAGE_COUNTER_ATTACK_TWO)
{
    $qb = $entityManager->createQueryBuilder();
    $qb->select('p')
        ->from('Player','p')
        ->where('p.enemycolonylevel = '.FINAL_WIN_LEVEL)
        ->andWhere('p.victory = true');
    $players = $qb->getQuery()->getResult();
    if (count($players) >= FINAL_WINS_REQUIRED)
    {
		$game->setStage(STAGE_HUMAN_VICTORY);
	}
}

$entityManager->flush();
