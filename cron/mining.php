<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$fleet = $player->getFleet();
	$sector = $player->getSector();
	$politicalsystem = $fleet->getPoliticalSystem();
	if (!is_null($sector))
	{
		$availableMaterial = $sector->getMaterial();
		$availableUranium = $sector->getUranium();
		if ( $availableMaterial > 0 || $availableUranium > 0)
		{
			$miningpower = 0;
			$ships = $fleet->getShips();
			$bonus=1;
			if (!is_null($politicalsystem))
			{
				$bonus = 1 + $politicalsystem->getProductionBonus();
			}
			foreach ($ships as $ship)
			{
				$miningpower += $ship->getType()->getMaterialProduction($ship->getLevel()) * $ship->getEfficiency() * $bonus;
			}
			$miningpower = round($miningpower);
			if ($availableMaterial >= $miningpower)
			{
				$sector->setMaterial($availableMaterial - $miningpower);
				$fleet->increaseMaterial($miningpower);
			}
			else
			{
				$sector->setMaterial(0); 
				$fleet->increaseMaterial($availableMaterial);
			}
			if ($availableUranium >= $miningpower)
			{
				$sector->setUranium($availableUranium - $miningpower);
				$fleet->setUranium($fleet->getUranium() + $miningpower);
			}
			else
			{
				$sector->setUranium(0);
				$fleet->setUranium($fleet->getUranium() + $availableUranium);
			}
		}
	}
}

$entityManager->flush();
