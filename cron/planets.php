<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

function prod_planet($planet,$initialColony=true)
{
	$player = null;
	if ($initialColony)
	{
		$player = $planet->getFreedBy();
	}
	else
	{
		$player = $planet->getColonizedBy();
	}
	if (!is_null($player))
	{
		$fleet = $player->getFleet();
		$production = $planet->getFoodProduction();
		$fleet->setFood($fleet->getFood() + $production);
		$production = $planet->getFuelProduction();
		$fleet->increaseFuel($production);
		$production = $planet->getUraniumProduction();
		$fleet->setUranium($fleet->getUranium() + $production);
		$production = $planet->getMaterialProduction();
		$fleet->increaseMaterial($production);
		$production = $planet->getMedicineProduction();
		$fleet->setMedicine($fleet->getMedicine() + $production);
		$production = $planet->getStaffProduction();
		$fleet->increaseQualifiedStaff($production);
	}
}

$planets = $entityManager->getRepository('Planet')->findAll();

foreach ($planets as $planet)
{
	$planet->setMaterial($planet->getMaterial() + $planet->getMaterialProduction());
    if ($planet->getInitialColony() && $planet->getMaterial() > 1000 && $planet->getStatus() == PLANET_STATUS_OCCUPIED && !$planet->isAttacked())
    {
        // @TODO : change this
        $nbAvailableShips = count($entityManager->getRepository('EnnemyShipType')->findAll());
		$randId = rand(1,$nbAvailableShips);
        
        $garrison = $planet->getGarrison();
        $garrison[$randId] = $garrison[$randId] + 1;
        $planet->setGarrison($garrison);
        
        $planet->setMaterial($planet->getMaterial() - 1000);
    }
	elseif ($planet->getInitialColony() && $planet->getStatus() == PLANET_STATUS_FREED)
	{
		prod_planet($planet,true);
	}
	elseif (!$planet->getInitialColony())
	{
		prod_planet($planet,false);
	}
}
$entityManager->flush();
