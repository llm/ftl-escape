<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');
require_once($docroot.'/lib/i18n.php');

exit;

$qb = $entityManager->createQueryBuilder();

$qb->select('p')
	->from('Player','p')
	->where('p.deletiondate < ?1')
	->setParameter(1,time()-DELETION_PERIOD);
	
$players = $qb->getQuery()->getResult();

$i18n = new I18n();
$i18n->autoSetLang();

foreach ($players as $player)
{
	$sector = $player->getSector();
	$fleet = $player->getFleet();
	
	$planet = $sector->getPlanet();
	
	if (!is_null($planet))
	{
		$planet->unattack();
		$ennemies = $sector->getEnnemies();
		if (count($sector->getEnnemies()) > 0)
		{
			$planet->setStatus(PLANET_STATUS_OCCUPIED);
		}
		$fleet->setJumpStatus(JUMP_STATUS_SAFE);
		Builder::jump($player);
	}
	$player->trueDeleteAccount();
	//$entityManager->remove($sector);
	if (!is_null($fleet))
	{
		$entityManager->remove($fleet);
	}
	//$entityManager->remove($player);
}
$entityManager->flush();
