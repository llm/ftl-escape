<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$fleet = $player->getFleet();
	$ships = $fleet->getShips();
	$politicalsystem = $fleet->getPoliticalSystem();
	$medicineproduction=0;
	$humans=0;
	foreach ($ships as $ship)
	{
		$humans += $ship->getPassengers() + $ship->getStaff();
		$medicineproduction += $ship->getType()->getMedicineProduction($ship->getLevel()) * $ship->getEfficiency();
	}
	$medicineproduction = round($medicineproduction);
	$bonus=1;
	if (!is_null($politicalsystem))
	{
		$bonus = 1 + $politicalsystem->getProductionBonus();
	}
	$medicineproduction = $medicineproduction * $bonus;
	$conso = round($humans / MEDICINE_CONSUMPTION);
	$left = $fleet->getMedicine() - $conso + $medicineproduction;
	if ($left < 0)
	{
		foreach($ships as $ship)
		{
			$leftPassengers = round($ship->getPassengers()*(1-KILL_PER_CENT_IN_DISEASE));
			$ship->setPassengers($leftPassengers);
			// medicine are a priority for staff
			/*
			$leftStaff = round($ship->getStaff()*(1-KILL_PER_CENT_IN_DISEASE));
			$ship->setStaff($leftStaff);
			*/
		}
		$left = 0;
	}
	$fleet->setMedicine($left);
	
}

$entityManager->flush();
