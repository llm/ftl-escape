<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$nexii = $entityManager->getRepository('Nexus')->findAll();

foreach ($nexii as $nexus)
{
	$nexus->reinitAttackCount();
}
$entityManager->flush();
