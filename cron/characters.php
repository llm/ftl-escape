<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');
require_once($docroot.'/builder.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$fleet = $player->getFleet();
    $characters = $fleet->getCharacters();
    if (count($characters) < NB_CHARACTERS)
    {
        $character = Builder::buildRandomCharacter($fleet);
        $entityManager->persist($character);
        $message = new Message(null,$player,'msg.new.character',true);
		$entityManager->persist($message);
    }
}
$entityManager->flush();