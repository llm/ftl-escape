<?php

$smarty->assign('game_level',$game->getStage());

$smarty->assign('players',$entityManager->getRepository('Player')->findAll());
$smarty->assign('planets',$entityManager->getRepository('Planet')->findBy(array('initialcolony'=>true)));
$smarty->assign('new_planets',$entityManager->getRepository('Planet')->findBy(array('initialcolony'=>false)));
$smarty->assign('nexii',$entityManager->getRepository('Nexus')->findAll());

// i18n
$smarty->assign('i18n',$i18n);
$smarty->assign('lbl_player',$i18n->getText('lbl.commander'));
$smarty->assign('lbl_nb_ships',$i18n->getText('lbl.nb.ships'));
$smarty->assign('lbl_nb_survivors',$i18n->getText('lbl.nb.survivors'));
$smarty->assign('lbl_galaxy',$i18n->getText('lbl.menu.galaxy'));
$smarty->assign('lbl_freed_by',$i18n->getText('lbl.freed.by'));
$smarty->assign('lbl_colonized_by',$i18n->getText('lbl.colonized.by'));
$smarty->assign('lbl_attacked_by',$i18n->getText('lbl.attacked.by'));
$smarty->assign('lbl_planets',$i18n->getText('lbl.planets'));
$smarty->assign('lbl_nexus',$i18n->getText('lbl.nexus'));
$smarty->assign('lbl_destroyed_by',$i18n->getText('lbl.destroyed.by'));
$smarty->assign('lbl_colony',$i18n->getText('lbl.enemy.colony'));
$smarty->assign('lbl_unknown',$i18n->getText('lbl.unknown'));
