<?php
$qb = $entityManager->createQueryBuilder();
$qb->select('p')
	->from('Planet','p')
	->where('p.freedBy = :player')
	->setParameter('player',$player);
$query = $qb->getQuery();
$planets = $query->getResult();

$smarty->assign('planets',$planets);

// I18n
$smarty->assign('lbl_planets',$i18n->getText('lbl.menu.planets'));
$smarty->assign('lbl_planet',$i18n->getText('lbl.planet'));
$smarty->assign('lbl_population',$i18n->getText('lbl.population'));
$smarty->assign('lbl_food',$i18n->getText('lbl.food'));
$smarty->assign('lbl_fuel',$i18n->getText('lbl.fuel'));
$smarty->assign('lbl_material',$i18n->getText('lbl.material'));
$smarty->assign('lbl_uranium',$i18n->getText('lbl.uranium'));
$smarty->assign('lbl_medicine',$i18n->getText('lbl.medicine'));
$smarty->assign('lbl_staff',$i18n->getText('lbl.staff'));
$smarty->assign('lbl_transfer',$i18n->getText('lbl.transfer'));