<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_GET['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	$shipid = $_GET['id'];
	if (!is_numeric($shipid))
	{
		echo "Nope.";
		exit;
	}

	if (!Helper::canAct($player))
	{
		Tools::setFlashMsg($i18n->getText('msg.not.allowed'));
	}
	else
	{
		$fleet = $player->getFleet();

		$ship = $fleet->getShip($shipid);

		if (is_null($ship))
		{
			echo "This ship does not seems to be in this fleet. Please go back.";
			exit;
		}

		$staffStock = $fleet->getQualifiedStaff();
		// if there is not enough "simple passengers" onboard, you cannot increase as much as staff you want
		if ($staffStock > $ship->getPassengers())
		{
			$staffStock = $ship->getPassengers();
			Tools::setFlashMsg($i18n->getText('msg.not.enough.passengers.for.staff'));
		}
		if ($staffStock == 0)
		{
			Tools::setFlashMsg($i18n->getText('msg.impossible.assignation'));
		}
		else
		{
			if ($ship->getType()->getQualifiedStaff() < $ship->getStaff() + $staffStock)
			{
				$currentStaff = $ship->getStaff();
				$ship->setStaff($ship->getType()->getQualifiedStaff());
				$fleet->decreaseQualifiedStaff($ship->getType()->getQualifiedStaff() - $currentStaff);
				$ship->setPassengers($ship->getPassengers() - ($ship->getType()->getQualifiedStaff() - $currentStaff));
			}
			else
			{
				$ship->setStaff($ship->getStaff() + $staffStock);
				$fleet->decreaseQualifiedStaff($staffStock);
				$ship->setPassengers($ship->getPassengers() - $staffStock);
			}
			Tools::setFlashMsg($i18n->getText('msg.staff.assigned'));
			$entityManager->flush();
		}
	}
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php?page=fleet');
