<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_POST['token']))
{
    if ($player->isVictorious() && $player->getObjectiveType() == OBJECTIVE_SEARCH_HABITABLE_ID)
    {
        $name = htmlspecialchars(strip_tags($_POST['pname']));
        if ($name != "")
        {
            $planet = new Planet($name,5);
            $planet->setInitialColony(false);
            $planet->setColonizedBy($player);
            $planet->setStatus(0);
            $entityManager->persist($planet);
            Tools::setFlashMsg($i18n->getText('msg.planet.created',array($name)));
            Builder::resetVictory($player);
        }
        else
        {
            Tools::setFlashMsg($i18n->getText('msg.invalid.name'));
        }
    }
}
$entityManager->flush();
header('Location:index.php');