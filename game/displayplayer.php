<?php
$dplayer = $entityManager->find('Player',$_GET['id']);
if (!$dplayer->isDeleted())
{
$smarty->assign('player',$dplayer);
$smarty->assign('badges',$dplayer->getBadges());
$smarty->assign('political_system',$dplayer->getFleet()->getPoliticalSystem());

// i18n
$smarty->assign('i18n',$i18n);
$smarty->assign('lbl_fleet',$i18n->getText('lbl.fleet'));
$smarty->assign('lbl_political_system',$i18n->getText('lbl.political.system'));
$smarty->assign('lbl_ships',$i18n->getText('word.ships'));
}
else
{
	$smarty->assign('msg_is_deleted',$i18n->getText('msg.player.deleted'));
}
