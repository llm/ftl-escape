/*
=================================================
                Sortable tables
=================================================
*/
// by eijebong
// License : WTFPL
/*DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
 * */

function isNumber(n) {
	return !isNaN(parseFloatWithSpaces(n)) /*&& isFinite(parseFloatWithSpaces(n))*/;
}

function parseFloatWithSpaces(f)
{
	f = f.replace(/\s/gi,'');
	return parseFloat(f);
}

function $A(iterable) {
	if (!iterable) return [];
	if ('toArray' in Object(iterable)) return iterable.toArray();
	var length = iterable.length || 0, results = new Array(length);
	while (length--) results[length] = iterable[length];
		return results;
}

function getArrowElement(th){
	childs_length = th.childNodes.length;
	for(var i=0; i < childs_length; i++){
		if(/arrow/.test(th.childNodes[i].className)){
			return th.childNodes[i];
		}
	}
}

function sortable_table(table_id,blacklist){
	if (blacklist == null)
	{
		blacklist=[];
	}
	var table = document.getElementById(table_id);
	var tbody = table.getElementsByTagName("tbody")[0]
	if(!table)
		return

	var ths = table.getElementsByTagName("th");
	var ths_length = ths.length;

	for(var i=0; i < ths_length; i++){

		var spanArrow = document.createElement("span");
		if (blacklist.indexOf(i) == -1)
		{
			spanArrow.className = "arrow both";
		}
		if(ths[i].childNodes[0]){
			ths[i].appendChild(spanArrow);
		}
		ths[i].addEventListener("click", function(event){
			var elem = null;
			elem = event.target;
			if(!elem){
				elem  = event.srcElement;
			}
			elem = elem.parentNode;
			if(elem.nodeName != "TH"){
				return;
			}

			for(var i=0; i < ths_length; i++){
				if(ths[i] != elem && blacklist.indexOf(i) == -1){
					if(ths[i].childNodes[0]){
						getArrowElement(ths[i]).className = "arrow both";
					}
				}
			}

			for (var k=0,e=elem; e = e.previousSibling; ++k){
				if(e.nodeType !== 1){
					k--;
				}
			}
			var trs_sortable = tbody.getElementsByTagName("tr");
			/*try{
				trs_sortable = Array.prototype.slice.call(trs_sortable, 1);
			}
			catch(e){
				trs_sortable = $A(trs_sortable);
				trs_sortable.slice(1);
			}*/

			var tds = Array();
			for(var j=0; j < trs_sortable.length; j++){
				tds.push(trs_sortable[j].getElementsByTagName("td")[k]);
			}

			function sort_by_name(a, b) {
				while(a.firstChild){
					a = a.firstChild;
				}
				while(b.firstChild){
					b = b.firstChild;
				}
				var sa = String(a.nodeValue);
				var sb = String(b.nodeValue);

				if(isNumber(sa) && isNumber(sb)){ if(direction == "down"){ return parseFloatWithSpaces(sb)<parseFloatWithSpaces(sa) ? -1 : parseFloatWithSpaces(sb)>parseFloatWithSpaces(sa) ? 1 : 0; } else{ return parseFloatWithSpaces(sb)<parseFloatWithSpaces(sa) ? 1 : parseFloatWithSpaces(sb)>parseFloatWithSpaces(sa) ? -1 : 0; } }
				if(direction == "down"){
					return sb<sa ? -1 : sb>sa ? 1 : 0;
				}
				else{
					return sb<sa ? 1 : sb>sa ? -1 : 0;
				}
			}
			var arrow = getArrowElement(elem);
			if(arrow.className == "arrow both" || arrow.className == "arrow down"){
				direction = "up";
				arrow.className = "arrow up";
			}
			else{
				direction = "down";
				arrow.className = "arrow down";
			}

			var a = Array.prototype.sort.call(tds, sort_by_name);
			for(var b in a){
				tbody.appendChild(a[b].parentNode)
			}
		});
	}
}
