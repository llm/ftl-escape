<?php

$id = $_GET['id'];
$ship = $player->getFleet()->getShip($id);
if (!empty($ship))
{
    $level = $ship->getLevel();
    $smarty->assign('not_in_ships',false);
    $smarty->assign('id',$ship->getId());
    $smarty->assign('name',$ship->getName());
    $smarty->assign('type',$ship->getType()->getName());
    $smarty->assign('damages',$ship->getType()->getMaxHP() - $ship->getHP());
    $smarty->assign('heavydamages',$ship->getHP() < $ship->getType()->getMaxHP() / 2);
    $smarty->assign('ftl',$ship->getFTLDrive());
    $smarty->assign('survivesystems',$ship->getSurviveSystems());
    $smarty->assign('hp',$ship->getHP());
    $smarty->assign('maxhp',$ship->getType()->getMaxHP($level));
    $smarty->assign('staff',$ship->getStaff());
    $smarty->assign('maxstaff',$ship->getType()->getQualifiedStaff());
    $smarty->assign('passengers',$ship->getPassengers());
    $smarty->assign('maxpassengers',$ship->getType()->getMaxPassengers());
    $smarty->assign('oxygenlevel',$ship->getOxygenLevel());
    $smarty->assign('level',Tools::roman_numerals($level+1));
    $smarty->assign('jumpstatus',$player->getFleet()->getJumpStatus());
    $smarty->assign('maxjumpstatus',JUMP_STATUS_SAFE); // we could use "MAX_JUMP_STATUS" but the FTL drive is ready when the jump is safe
    $smarty->assign('defense',$ship->getDefense($level));
    $smarty->assign('attack',$ship->getAttack($level));
    $smarty->assign('type_attack',$ship->getType()->getAttack());
    $smarty->assign('type_defense',$ship->getType()->getDefense());
    
    // i18n
    $smarty->assign('type_name',$i18n->getText($ship->getType()->getName()));
    $smarty->assign('lbl_class',$i18n->getText('lbl.class'));
    $smarty->assign('lbl_hp',$i18n->getText('lbl.ship.hp'));
    $smarty->assign('lbl_staff',mb_convert_case($i18n->getText('lbl.staff'),MB_CASE_TITLE));
    $smarty->assign('lbl_passengers',mb_convert_case($i18n->getText('lbl.passengers'),MB_CASE_TITLE));
    $smarty->assign('lbl_attack',$i18n->getText('lbl.ship.attack'));
    $smarty->assign('lbl_defense',$i18n->getText('lbl.ship.defense'));
    $smarty->assign('lbl_return',$i18n->getText('lbl.return'));
    $smarty->assign('lbl_normal_type_value',$i18n->getText('lbl.normal.type.value'));
}
else
{
    $smarty->assign('not_in_ships',true);
}