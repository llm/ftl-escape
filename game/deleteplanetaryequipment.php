<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();
$pid = null;
if (Helper::checkCSRF($_GET['token']))
{
    $equipment = $entityManager->find('PlanetaryEquipment',$_GET['id']);
    $planet = $equipment->getPlanet();
    $pid = $planet->getId();
    if ($planet->getFreedBy()->getId() == $player->getId())
    {
        $fleet = $player->getFleet();
        $fleet->increaseMaterial(round($equipment->getType()->getPrice()/2,0));
        $entityManager->remove($equipment);
    }
    $entityManager->flush();
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
if (is_null($pid))
{
    header('Location: index.php?page=planets');
}
else
{
    header('Location: index.php?page=planet&id='.$pid);
}
