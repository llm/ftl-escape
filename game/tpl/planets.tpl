<h1>{$lbl_planets}</h1>
<div class="datagrid">
    <table>
        <thead><tr><th>{$lbl_planet}</th><th>{$lbl_population}</th><th>{$lbl_food}</th><th>{$lbl_fuel}</th><th>{$lbl_material}</th><th>{$lbl_uranium}</th><th>{$lbl_medicine}</th><th>{$lbl_staff}</th></tr></thead>
        <tbody>
            {$food=0}
            {$fuel=0}
            {$material=0}
            {$uranium=0}
            {$medicine=0}
            {$staff=0}
        {foreach $planets as $planet}
        <tr>
            <td><a href="index.php?page=planet&id={$planet->getId()}">{$planet->getName()}</a></td>
            <td>{$planet->getPopulation()}</td>
            <td>{$food=$food+$planet->getFoodProduction()}{$planet->getFoodProduction()}</td>
            <td>{$fuel=$fuel+$planet->getFuelProduction()}{$planet->getFuelProduction()}</td>
            <td>{$material=$material+$planet->getMaterialProduction()}{$planet->getMaterialProduction()}</td>
            <td>{$uranium=$uranium+$planet->getUraniumProduction()}{$planet->getUraniumProduction()}</td>
            <td>{$medicine=$medicine+$planet->getMedicineProduction()}{$planet->getMedicineProduction()}</td>
            <td>{$staff=$staff+$planet->getStaffProduction()}{$planet->getStaffProduction()}</td>
        </tr>
        {/foreach}
        </tbody>
        <tfoot><tr><td></td><td><a href="index.php?page=ptransfer">{$lbl_transfer}</a></td><td>{$food} / h</td><td>{$fuel} / h</td><td>{$material} / h</td><td>{$uranium} / h</td><td>{$medicine} / h</td><td>{$staff} / h</td></tr></tfoot>
    </table>
</div>
