<h1>{$lbl_victory}</h1>
<p>{$victory_txt}</p>
{if $display_planet_form}
<p>
    <form action="createplanet.php" method="post">
        <input type="hidden" name="token" value="{$token}"/>
        {$lbl_name_planet} : <input type="text" name="pname"/>
        <input type="submit" value="{$lbl_name}"/>
    </form>
</p>
{/if}
{if !$final_victory}<p><a href="resetobjective.php?token={$token}">{$lbl_reset_objective}</a></p>{/if}
