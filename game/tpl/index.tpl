<!DOCTYPE html>
<html>
<head>
	<title>{$title}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=yes"/>
	<link rel="stylesheet" href="css/main.css" type="text/css"/>
	<script type="text/javascript">

	function goLite(FRM,BTN)
	{
	   window.document.forms[FRM].elements[BTN].style.backgroundColor = "#336600";
	}

	function goDim(FRM,BTN)
	{
	   window.document.forms[FRM].elements[BTN].style.backgroundColor = "#990000";
	}

	</script>
	<script type="text/javascript" src="js/sortable_table.js"></script>
	<script type="text/javascript" src="js/sort_select.js"></script>

</head>
<body>
	{if $helpcontent}
	<script type="text/javascript">
	function showhelp(){
		document.getElementById('help-content').style.display = 'block';
	}
	
	function closehelp(){
		document.getElementById('help-content').style.display = 'none';
	}
	</script>
		<div id="help-content" style="display:none;">
			<div id="help-close"><a href="#" onclick="closehelp();"><img src="/game/img/close.png"/></a></div>
			<div id="help-content-desc">{$helpcontent}</div>
		</div>
	{/if}
	<div id="header">{include file="header.tpl"}</div>
	<div id="body-content">
		{if $helpcontent}
			<div id="help"><a href="#" onclick="showhelp();"><img src="/game/img/help.png"/></a></div>
		{/if}
		<div id="menu">{include file="menu.tpl"}</div>
		<div id="content">
		{$content}
		</div>
		<br />
	</div>
	<div id="footer">{include file="footer.tpl"}</div>
</body>
</html>
