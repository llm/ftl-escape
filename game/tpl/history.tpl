<h1>{$lbl_lost_ships}</h1>
<div class="datagrid">
	<table id="tbl_h">
		<thead><tr><th>{$hist_th_name}</th><th>{$hist_th_passengers}</th><th>{$hist_th_killedby}</th><th>{$hist_th_date}</th></tr></thead>
		<tbody>
		{$nblost=0}
		{foreach $history as $item}
		{$nblost = $nblost + $item->getPassengers() + $item->getStaff()}
			<tr><td>{$item->getName()} ({$i18n->getText($item->getType()->getName())})</td><td>{$item->getPassengers() + $item->getStaff()} ({$item->getStaff()})</td><td>{if !is_null($item->getKiller())}{$i18n->getText($item->getKiller()->getName())}{else}N/A{/if}</td><td>{$item->getTime()|date_format:"%d/%m/%y %H:%M:%S"}</td></tr>
		{/foreach}
		<tfoot><tr><td></td><td>{$nblost}</td><td></td><td></td></tr></tfoot>
		</tbody>
		</table>
</div>
<script type="text/javascript" src="js/pagination.js">
</script>
<script type="text/javascript">
var notifications = document.getElementById('tbl_h');
var options = {
    perPage: 20,
    perPageSelect: [5,10,15,20,25],
    navPosition: 'bottom',
    nextPrev: true,
    prevText: '&lsaquo;',
    nextText: '&rsaquo;',
    sortable: true,
    searchable: false,
    fixedHeight: false,
    info: false,
    hideUnusedNavs: false,
};
new DataTable(notifications, options);
</script>
