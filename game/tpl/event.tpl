<h1>{$lbl_event}</h1>

{$event_name}<br />
{$event_description}<br />
{$lbl_event_question}<br />

<form action="resolveevent.php" method="post">
	<input type="hidden" name="token" value="{$token}"/>
	<ul id="event_answers">
{foreach $event_answers as $id=>$answer}
	<li><input type="radio" name="answer" value="{$id}" />
	{$answer_effects = $answer['effects']}
	{$i18n->getText($answer['name'])}
	({$lbl_event_result} : 
	{$count = count($answer_effects)}
	{if empty($answer_effects)}
	{$lbl_nothing}
	{/if}
	{if array_key_exists('moral',$answer_effects)}
	{$answer_effects['moral']['min']} - {$answer_effects['moral']['max']} {$lbl_moral}{$count = $count-1}{if $count > 0},{/if}
	{/if}
	{if array_key_exists('food',$answer_effects)}
	{$answer_effects['food']['min']} - {$answer_effects['food']['max']} {$lbl_food}{$count = $count-1}{if $count > 0},{/if}
	{/if}
	{if array_key_exists('fuel',$answer_effects)}
	{$answer_effects['fuel']['min']} - {$answer_effects['fuel']['max']} {$lbl_fuel}{$count = $count-1}{if $count > 0},{/if}
	{/if}
	{if array_key_exists('medicine',$answer_effects)}
	{$answer_effects['medicine']['min']} - {$answer_effects['medicine']['max']} {$lbl_medicine}{$count = $count-1}{if $count > 0},{/if}
	{/if}
	{if array_key_exists('material',$answer_effects)}
	{$answer_effects['material']['min']} - {$answer_effects['material']['max']} {$lbl_material}{$count = $count-1}{if $count > 0},{/if}
	{/if}
	{if array_key_exists('staff',$answer_effects)}
	{$answer_effects['staff']['min']} - {$answer_effects['staff']['max']} {$lbl_staff}{$count = $count-1}{if $count > 0},{/if}
	{/if}
	{if array_key_exists('passengers',$answer_effects)}
	{$answer_effects['passengers']['min']} - {$answer_effects['passengers']['max']} {$lbl_passengers}{$count = $count-1}{if $count > 0},{/if}
	{/if}
	{if array_key_exists('ships',$answer_effects)}
	{$answer_effects['ships']['min']} - {$answer_effects['ships']['max']} {$lbl_ships}{$count = $count-1}{if $count > 0},{/if}
	{/if}
	{if array_key_exists('ennemies',$answer_effects)}
	{$lbl_possible_ennemies}{$count = $count-1}{if $count > 0},{/if}
	{/if}
	{if array_key_exists('nbshipsdamaged',$answer_effects)}
	{$answer_effects['nbshipsdamaged']['min']} - {$answer_effects['nbshipsdamaged']['max']} {$lbl_nb_ships_damaged} ({$answer_effects['damages']['min']} - {$answer_effects['damages']['max']} {$lbl_damages}){$count = $count-2}{if $count > 0},{/if}
	{/if}
	{if array_key_exists('possibleambush',$answer_effects)}
	{$lbl_possible_allies}, {$lbl_possible_ennemies}{$count = $count-1}{if $count > 0},{/if}
	{/if}
	)
	</li>
{/foreach}
</ul>
<input type="submit" value="{$lbl_solve}"/>
</form>
