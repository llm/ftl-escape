<h1>{$lbl_transfer_title}</h1>
{if $can_act}
<div class="datagrid">
	<form action="movepeopletoplanet.php" method="post">
		<input type="hidden" name="token" value="{$token}"/>
	<table id="tbl_fleet">
		<thead><tr><th>{$lbl_ship_name}</th><th>{$lbl_ship_type_name}</th><th>{$lbl_ship_passengers}</th><th>{$lbl_operation}</th></tr></thead>
		<tfoot><tr><th>{$lbl_nb_to_dispatch}</th><th></th><th id="passengers_to_dispatch">0</th></tr></tfoot>
		<tbody>
			{foreach $ships as $ship}
				<tr id="ship{$ship->getId()}"><td>{$ship->getName()}</td><td>{$i18n->getText($ship->getType()->getName())}</td>
				<td id="passengers{$ship->getId()}"><span id="nbpassengers{$ship->getId()}">{$ship->getPassengers()}</span><input type="hidden" id="valnbpassengers{$ship->getId()}" name="valnbpassengers[{$ship->getId()}]" value="{$ship->getPassengers()}" />/<span id="maxp{$ship->getId()}">{$ship->getType()->getMaxPassengers()}</span></td>
				<td> 
					<a href="#" onclick="return removePeople('p',{$ship->getId()},100)">-100</a>/
					<a href="#" onclick="return removePeople('p',{$ship->getId()},50)">-50</a>/
					<a href="#" onclick="return removePeople('p',{$ship->getId()},1)">-1</a>/
					<a href="#" onclick="return addPeople('p',{$ship->getId()},{$ship->getType()->getMaxPassengers()},1)">+1</a>/
					<a href="#" onclick="return addPeople('p',{$ship->getId()},{$ship->getType()->getMaxPassengers()},50)">+50</a>/
					<a href="#" onclick="return addPeople('p',{$ship->getId()},{$ship->getType()->getMaxPassengers()},100)">+100</a>
				</td>
				</tr>
			{/foreach}
		</tbody>
	</table>
    <table>
        <thead><tr><th>Planète</th><th>Population</th><th>{$lbl_operation}</th></tr></thead>
        <tbody>
            {foreach $planets as $planet}
            <tr id="planet{$planet->getId()}"><td>{$planet->getName()}</td><td id="population{$planet->getId()}"><span id="nbpopulation{$planet->getId()}">{$planet->getPopulation()}</span><input type="hidden" id="valnbpopulation{$planet->getId()}" name="valnbpopulation[{$planet->getId()}]" value="{$planet->getPopulation()}"/></td>
            <td>
            <a href="#" onclick="return removePeople('s',{$planet->getId()},100)">-100</a>/
					<a href="#" onclick="return removePeople('s',{$planet->getId()},50)">-50</a>/
					<a href="#" onclick="return removePeople('s',{$planet->getId()},1)">-1</a>/
					<a href="#" onclick="return addPeople('s',{$planet->getId()},10000000000,1)">+1</a>/
					<a href="#" onclick="return addPeople('s',{$planet->getId()},10000000000,50)">+50</a>/
					<a href="#" onclick="return addPeople('s',{$planet->getId()},10000000000,100)">+100</a>
            </td>
            </tr>
            {/foreach}
        </tbody>
    </table>
	<input type="submit" value="{$lbl_transfer}" onclick="return check_transfer();"/>
	</form>
</div>
{else}

{/if}
<script type="text/javascript">
	function removePeople(type,id,nb)
	{
		idvalue = null;
		idnb = null;
		iddispatch = null;
		if (type == 'p')
		{
			idvalue='valnbpassengers';
			idnb = 'nbpassengers';
			iddispatch = 'passengers_to_dispatch';
		}
		else if (type == 's')
		{
			idvalue='valnbpopulation';
			idnb = 'nbpopulation';
			iddispatch = 'passengers_to_dispatch';
		}
		nbpassengers = parseInt(document.getElementById(idvalue+id).value);
		newvalue = nbpassengers - nb;
		if (newvalue < 0)
		{
			newvalue = 0;
			nb = nbpassengers;
		}
		document.getElementById(idvalue+id).value = newvalue;
		document.getElementById(idnb+id).innerHTML = newvalue;
		dispatch = parseInt(document.getElementById(iddispatch).innerHTML);
		dispatch += nb;
		document.getElementById(iddispatch).innerHTML = dispatch;
		return false;
	}

	function addPeople(type,id,maximum,nb)
	{
		idvalue = null;
		idnb = null;
		iddispatch = null;
		if (type == 'p')
		{
			idvalue='valnbpassengers';
			idnb = 'nbpassengers';
			iddispatch = 'passengers_to_dispatch';
		}
		else if (type == 's')
		{
			idvalue='valnbpopulation';
			idnb = 'nbpopulation';
			iddispatch = 'passengers_to_dispatch';
		}
		nbpassengers = parseInt(document.getElementById(idvalue+id).value);
		newvalue = nbpassengers + nb;
		if (newvalue > maximum)
		{
			newvalue = maximum;
			nb = maximum - nbpassengers;
		}
		document.getElementById(idvalue+id).value = newvalue;
		document.getElementById(idnb+id).innerHTML = newvalue;
		dispatch = parseInt(document.getElementById(iddispatch).innerHTML);
		dispatch -= nb;
		document.getElementById(iddispatch).innerHTML = dispatch;
		return false;
	}

	function check_transfer()
	{
		dispatchp = parseInt(document.getElementById('passengers_to_dispatch').innerHTML);
		dispatchs = parseInt(document.getElementById('passengers_to_dispatch').innerHTML);
		return dispatchp == 0 && dispatchs == 0;
	}
</script>

