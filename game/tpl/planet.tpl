<h1>{$planet->getName()}</h1>
Population : {$planet->getPopulation()}<br />
{$equipments = $planet->getEquipments()}
Equipments ({count($equipments)})<br />
<ul>
    {foreach $equipments as $equipment}
    <li>{$i18n->getText($equipment->getType()->getName())} ({$equipment->getHP()}/{$equipment->getType()->getMaxHP()}) <a class="tooltip clickable" href="deleteplanetaryequipment.php?id={$equipment->getId()}&token={$token}">X<span class="classic"></span></a></li>
    {/foreach}
</ul>
{if count($planet->getEquipments()) < $planet->getMaxEquipments()}
<form action="buildplanetaryequipment.php" method="post">
    <input type="hidden" name="token" value="{$token}"/>
    <input type="hidden" name="id" value="{$planet->getId()}"/>
<select name="equipmentid">
    {foreach $available_equipments as $equipment}
    <option value="{$equipment->getId()}">{$i18n->getText($equipment->getName())} ({$equipment->getPrice()})</option>
    {/foreach}
</select>
<input type="submit"/>
</form>
{/if}