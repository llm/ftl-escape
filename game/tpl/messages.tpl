<h1>{$lbl_received_messages}</h1>
<div class="datagrid">
	<table id="notifications">
		<thead><tr><th>{$messages_th_message}</th><th>{$messages_th_date}</th></tr></thead>
		<tbody>
		{foreach $notifications as $notification}
		{$read = $notification->isRead()}
			<tr class="message-{if !$read}un{/if}read"><td>{$notification->getMessage($i18n)}</td><td>{$notification->getTime()}</td></tr>
		{foreachelse}
			<tr><td colspan=2>{$messages_no_notif}</td></tr>
		{/foreach}
		</tbody>
	</table>
</div>
<div class="datagrid">
	<table id="messages">
	<thead><tr><th>{$messages_th_sender}</th><th>{$messages_th_message}</th><th>{$messages_th_date}</th></tr></thead>
	<tbody>
	{foreach $messages as $message}
	{$read = $message->isRead()}
		<tr class="message-{if !$read}un{/if}read"><td>{$message->getSender()->getLogin()}</td><td><a href="index.php?page=message&amp;id={$message->getId()}">{$message->getTruncatedMessage({$length})}</a></td><td>{$message->getTime()}</td></tr>
	{foreachelse}
		<tr><td colspan=3>{$messages_no_message}</td></tr>
	{/foreach}
	</tbody>
	</table>
</div>
<div id="sendmessage">
<form action="sendmessage.php" method="post">
	<input type="hidden" name="token" value="{$token}"/>
	<fieldset>
	<legend>{$messages_send}</legend>
	<dl>
		<dt>{$messages_recipient}</dt>
		<dd><input type="text" name="recipient" id="recipient" /></dd>
	</dl>
	<dl>
		<dt>{$messages_th_message}</dt>
		<dd><textarea name="message" id="message"></textarea></dd>
	</dl>
	<input class="action-button" type="submit" value="Envoyer"/>
	</fieldset>
</form>
</div>
<script type="text/javascript" src="js/pagination.js">
</script>
<script type="text/javascript">
var notifications = document.getElementById('notifications');
var messages = document.getElementById('messages');
var options = {
    perPage: 10,
    perPageSelect: [5,10,15,20,25],
    navPosition: 'bottom',
    nextPrev: true,
    prevText: '&lsaquo;',
    nextText: '&rsaquo;',
    sortable: false,
    searchable: false,
    fixedHeight: false,
    info: false,
    hideUnusedNavs: false,
};
new DataTable(notifications, options);
new DataTable(messages, options);
</script>
<script type="text/javascript" src="js/auto-complete.min.js"></script>
<script type="text/javascript">
        var recipients = new autoComplete({
            selector: '#recipient',
            minChars: 1,
            source: function(term, suggest){
                term = term.toLowerCase();
                var choices = [
                {foreach $players as $player}
                '{$player->getLogin()}',
                {/foreach}
                ];
                var suggestions = [];
                for (i=0;i<choices.length;i++)
                    if (~choices[i].toLowerCase().indexOf(term)) suggestions.push(choices[i]);
                suggest(suggestions);
            }
        });
</script>
