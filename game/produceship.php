<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');


if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	$shipid = $_POST['shipid'];

	if (!is_numeric($shipid))
	{
		echo "Nope.";
		exit;
	}

	$nbShip = $_POST['nbShip'];

	if (!is_numeric($nbShip) || $nbShip < 1)
	{
			echo "Nope.";
			exit;
	}

	$fleet = $player->getFleet();

	if (Helper::canAct($player) && Helper::canProduce($fleet))
	{

		$shipType = $entityManager->getRepository('ShipType')->find($shipid);
		
		if (!$shipType->isBuildable())
		{
			Tools::setFlashMsg($i18n->getText('msg.impossible.action'));
			header('Location: index.php?page=fleet');
			exit;
		}

		if ($shipType->getPrice() * $nbShip > $fleet->getMaterial())
		{
			Tools::setFlashMsg($i18n->getText('msg.not.enough.material'));
			header('Location: index.php?page=fleet');
			exit;
		}
		while($nbShip > 0)
		{
			Builder::buildShip($fleet,$shipType);
			$fleet->decreaseMaterial($shipType->getPrice());
			$nbShip--;
		}
		Tools::setFlashMsg($i18n->getText('msg.new.ship.created',array($i18n->getText($shipType->getName()))));
	}
	else
	{
		Tools::setFlashMsg($i18n->getText('msg.cannot.produce'));
	}
	$entityManager->flush();
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header('Location: index.php?page=fleet');
