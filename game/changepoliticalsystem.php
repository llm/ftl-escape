<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_POST['token']))
{
    if (array_key_exists('id',$_POST))
    {
        $username = $_SESSION['username'];
        $player = $entityManager->getRepository('Player')->findOneByLogin($username);
        $fleet = $player->getFleet();
        $politicalsystem = $fleet->getPoliticalSystem();
        if (is_null($politicalsystem) && is_numeric($_POST['id']))
        {
            $politicalsystem = $entityManager->find('PoliticalSystem',$_POST['id']);
            $fleet->setPoliticalSystem($politicalsystem);
            $entityManager->flush();
        }
    }
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header ('Location: index.php?page=politics');