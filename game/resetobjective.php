<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_GET['token']))
{
    if ($player->isVictorious() && $player->getEnemyColonyLevel() == 0)
    {
        Builder::resetVictory($player);
    }
}
$entityManager->flush();
header('Location: index.php');
