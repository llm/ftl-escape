<?php

$objective = $player->getObjectiveType();

$victory_txt='';
if ($game->getStage() == STAGE_HUMAN_VICTORY)
{
	$victory_txt = 'txt.victory.final.global';
}
elseif ($objective == OBJECTIVE_SEARCH_HABITABLE_ID)
{
	$victory_txt = 'txt.victory.habitable';
}
elseif ($objective == OBJECTIVE_SEARCH_EARTH_ID)
{
	$victory_txt = 'txt.victory.earth';
}
elseif ($game->getStage() == STAGE_COUNTER_ATTACK_TWO)
{
	$victory_txt = 'txt.victory.final.self';
}
else
{
	$victory_txt = 'nope';
}

$smarty->assign('fleet',$player->getFleet());
$smarty->assign('final_victory',$game->getStage() == STAGE_COUNTER_ATTACK_TWO || $game->getStage() == STAGE_HUMAN_VICTORY);

// I18n
$smarty->assign('lbl_victory',$i18n->getText('lbl.victory'));
$smarty->assign('victory_txt',$i18n->getText($victory_txt));
$smarty->assign('lbl_reset_objective',$i18n->getText('lbl.reset.objective'));
$smarty->assign('display_planet_form',$objective == OBJECTIVE_SEARCH_HABITABLE_ID);
$smarty->assign('lbl_name_planet',$i18n->getText('lbl.name.planet'));
$smarty->assign('lbl_name',$i18n->getText('lbl.name'));

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
