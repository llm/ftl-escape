<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

$i18n = new I18n();
$i18n->autoSetLang();

if (Helper::checkCSRF($_GET['token']))
{
	$shipid = $_GET['id'];
	if (!is_numeric($shipid))
	{
		echo "Nope.";
		exit;
	}
	
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);
	
	$fleet = $player->getFleet();
	
	$ship = $fleet->getShip($shipid);

	if (is_null($ship))
	{
		echo "This ship does not seems to be in this fleet. Please go back.";
		exit;
	}
	
	if ($ship->getPassengers() == 0 && $ship->getStaff() == 0 && count($ship->getCharacters()) == 0)
	{
		$materials = $ship->getType()->getPrice() * RECYCLE_SHIP_REFUND + $ship->getType()->getPrice() * SHIP_UPGRADE_PRICE * RECYCLE_SHIP_REFUND * $ship->getLevel();
		
		$fleet->increaseMaterial($materials);
		$entityManager->remove($ship);
		
		$entityManager->flush();
		Tools::setFlashMsg($i18n->getText('msg.ship.recycled',array($materials)));
	}
	else
	{
		Tools::setFlashMsg($i18n->getText('msg.ship.not.empty'));
	}
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.wrong.token'));
}
header('Location: index.php?page=fleet');
