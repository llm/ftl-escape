<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once __DIR__.'/../bootstrap.php';
require_once __DIR__.'/../const.php';
require_once __DIR__.'/../builder.php';
require_once __DIR__.'/../helper.php';

if (Helper::checkCSRF($_GET['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);
	
	$game = $entityManager->getRepository('Game')->find(1);

	if ($player->isGameOver() && !$game->getPermanentDeath())
	{
		Builder::restart($player);
		
		$entityManager->flush();
	}
	elseif ($game->getPermanentDeath())
	{
		Tools::setFlashMsg('msg.permanent.death');
	}
	else
	{
		echo "It's not finished for you yet.";
	}
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php');
